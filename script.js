let movies = [
				{
				title: 'Avengers',
				genre: 'Sci-Fi',
				releasedDate: date = new Date(2008, 04, 30),
				rating: 100,
             	showAllMovies: function(){
                console.log(this.title + " is an " + this.genre + " movie")
        		}
        		},

                {
                title: 'The Grudge',
				genre: 'Horror',
				releasedDate: date = new Date(2020, 01, 01),				
				rating: 89,
             	showAllMovies: function(){
                console.log(this.title + " is an " + this.genre + " movie")
        		}
        		},

                {
                title: '50 First Dates',
				genre: 'Rom-Com',
				releasedDate: date = new Date(2004, 02, 13),
				rating: 97,
             	showAllMovies: function(){
                console.log(this.title + " is an " + this.genre + " movie")
        		}
                },

                {
                title: 'Justice League',
				genre: 'Sci-Fi',
				releasedDate: date = new Date(2017, 11, 16),
				rating: 100,
             	showAllMovies: function(){
                console.log(this.title + " is an " + this.genre + " movie")
        		}
                },

                {
                title: "Don't Breathe",
				genre: 'Suspense',
				releasedDate: date = new Date(2016, 08, 26),
				rating: 100,
             	showAllMovies: function(){
                console.log(this.title + " is an " + this.genre + " movie")
        		}
                }
			]



for (i = 0; i < movies.length; i++){
		movies[i].showAllMovies()
  	}

//test
console.log('The movie ' + movies[0].title + ' has ' + movies[0].rating.rate + ' stars.');
console.log('The movie ' + movies[1].title + ' has ' + movies[1].rating + ' stars.');
console.log('The movie ' + movies[2].title + ' has ' + movies[2].rating + ' stars.');
console.log('The movie ' + movies[3].title + ' has ' + movies[3].rating + ' stars.');
console.log('The movie ' + movies[4].title + ' has ' + movies[4].rating + ' stars.');


